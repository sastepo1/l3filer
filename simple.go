package l3filer

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
)

func CountFileRows(dirPath string, fileName string) (int, error) {
	os.Chdir(dirPath)
	fileHandle, err := os.Open(fileName)
	if err != nil {
		return 0, fmt.Errorf("failed to open file: %s", err)
	}
	defer fileHandle.Close()

	buf := make([]byte, 32*1024)
	count := 1
	lineSep := []byte{'\n'}

	for {
		c, err := fileHandle.Read(buf)
		count += bytes.Count(buf[:c], lineSep)

		if err == io.EOF {
			return count, nil
		} else if err != nil {
			return 0, err
		}
	}
}

func CountFileWords(dirPath string, fileName string) (int, error) {
    os.Chdir(dirPath)
    fileHandle, err := os.Open(fileName)
	if err != nil {
		return 0, fmt.Errorf("failed to open file: %s", err)
	}
	defer fileHandle.Close()

	fileScanner := bufio.NewScanner(fileHandle)
	fileScanner.Split(bufio.ScanWords)
	count := 0

	for fileScanner.Scan() {
		count++
	}

	if err := fileScanner.Err(); err != nil {
		return 0, fmt.Errorf("failed to open file: %s", err)
	} else {
		return count, nil
	}
}
